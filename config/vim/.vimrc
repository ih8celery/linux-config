set nocompatible
filetype off
set wildmenu
set number
set linebreak
set showbreak=+++
set textwidth=80
set showmatch
set visualbell
set hlsearch
set smartcase
set ignorecase
set incsearch
set autoindent
set expandtab
set shiftwidth=2
set smartindent
set smarttab
set softtabstop=2
set ruler
set showtabline=2
set undolevels=1000
set mouse=a
set foldmethod=manual

let mapleader=','

nnoremap <leader>f :YcmCompleter FixIt<CR>
nnoremap <leader>d :YcmCompleter GoToDefinition<CR>
nnoremap <leader>i :YcmCompleter GoToInclude<CR>
nnoremap <leader>t :YcmCompleter GetType<CR>
nnoremap <leader>w :YcmCompleter GetDoc<CR>

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'ervandew/supertab'
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'ycm-core/YouCompleteMe'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'shougo/vimproc.vim'
Plugin 'tpope/vim-repeat'
Plugin 'godlygeek/tabular'
Plugin 'kovisoft/slimv'

let g:ycm_key_list_select_completion=['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion=['<C-p>', '<Up>']
let g:SuperTabDefaultCompletionType='<C-n>'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:ycm_global_ycm_extra_conf='~/.vim/bundle/YouCompleteMe/third_party/ycmd/.ycm_extra_conf.py'

call vundle#end()

filetype plugin indent on
