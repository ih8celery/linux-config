CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
PROJECT(lispr)

set(lispr_MAJOR_VERSION 0)
set(lispr_MINOR_VERSION 1)

set(PROJECT_ROOT /home/dodonian/proj/lispr)
set(lispr_SRC ${PROJECT_ROOT}/lib/src)
set(lispr_INCLUDE ${PROJECT_ROOT}/lib/include)

include_directories(${lispr_INCLUDE})

add_executable(lispr ${lispr_SRC}/lispr.cpp)
