(defvar *state* '(sad unhappy miserable poor))
(defvar *ops* '(eat drink))

(defstruct op (action nil) (preconds nil) (include nil) (exclude nil))

(defun gps (goals)
  (if (every #'achieve goals) 'solved 'unsolved))

(defun achieve (goal)
  (or (achieve? goal)
      (some #'apply-op (find-all goal *ops* :test #'appropriate?))))

(defun appropriate? (goal op)
  (member goal (op-include op)))

(defun achieve? (goal &key (state *state*))
  (member goal state))

(defun apply-op (op)
  (when (every #'achieve (op-preconds op))
    (print (list 'executing (op-action op)))
    (setf *state* (set-difference *state* (op-exclude op)))
    (setf *state* (union *state* (op-include op)))
    t))

