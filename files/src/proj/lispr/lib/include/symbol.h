
#include <string>
#include <iostream>
#include <variant>
#include <fstream>

namespace symbol {
  using SymbolStreamSourceType = std::variant<std::string, std::ifstream>;

  enum class SymbolType { PUNCTUATION, NAME };


}
