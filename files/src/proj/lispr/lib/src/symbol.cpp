/*
 *  symbol.cpp
 *
 *  define the types and functions for processing a stream of text into a
 *  stream of lispr symbols
 */

#include "symbol.h"

namespace symbol {
  Symbol::Symbol() = default;

  Symbol::Symbol(SymbolType& t): type(t), value(SymbolValue()) {}

  Symbol::Symbol(SymbolType& t, std::string& v): type(t), value(SymbolValue(v)) {}

  Symbol::Symbol(SymbolType& t, SymbolValue& v): type(t), value(SymbolValue(v)) {}

  Symbol::~Symbol() = default;

  SymbolType Symbol::getType() { return this.type; }

  SymbolValue Symbol::getValue() { return this.value; }

  bool operator==(Symbol& s1, Symbol& s2) {
    return (s1.get_type() == s2.get_type()) && (s1.get_value() == s2.get_value());
  }

  bool operator==(SymbolValue& v1, SymbolValue& v2) { return v1.string_value == v2.string_value; }

  SymbolStream::SymbolStream() = delete;

  SymbolStream::SymbolStream(std::ifstream& f): source(SymbolStreamSourceType(f)) {}

  SymbolStream::SymbolStream(std::string& s): source(SymbolStreamSourceType(s)) {}

  SymbolStream::~SymbolStream() {
    if (std::holds_alternative<std::ifstream>(this.source)) {
      std::get<std::ifstream>(this.source).close();
    }
  }

  static SymbolStream SymbolStream::from_string(const std::string& str) { return SymbolStream(); }

  static SymbolStream SymbolStream::from_file(const std::string& fname) { return SymbolStream(); }

  Symbol SymbolStream::get_symbol() { return Symbol(); }

  SymbolStream& operator>>(SymbolStream& src, Symbol& sym) {
    sym = src.get_symbol();
  }
