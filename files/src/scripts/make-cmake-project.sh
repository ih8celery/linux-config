#!/bin/bash

# cmake variables
CURRENT_CMAKE_VERSION=2.6
CMAKE_FILE=CMakeLists.txt

cmake_src_root=$HOME/.cmake_projects
project_src_root=$HOME/proj

# command line args
project_name=${1}

if [ -z "${project_name}" ]; then
  echo "project name is required"
  exit -1
elif [ "${project_name}" = "-h" -o "${project_name}" = "--help" ]; then
  echo "usage: mkcmake [-h]? [name]"
  exit 0
fi

echo "Creating CMake project"
echo "Project name is ${project_name}"
echo "Using CMake version ${CURRENT_CMAKE_VERSION}"
echo "CMake source home is ${cmake_src_root}"
echo "Projects source home is ${project_src_root}"

# make project dir
cmake_src_dir="${cmake_src_root}/${project_name}"
project_src_dir="${project_src_root}/${project_name}"

echo "CMake source location is ${cmake_src_dir}"
echo "Project source location is ${project_src_dir}"

if [ -d "${cmake_src_dir}" ]; then
  echo "CMake source location exists. Deleting..."
  rm -rf $cmake_src_dir
fi

if [ -d "${project_src_dir}" ]; then
  echo "Project source location exists. Deleting..."
  rm -rf $project_src_dir
fi

echo "Making CMake source location"
mkdir $cmake_src_dir

echo "Making project source subdirectories"
mkdir $project_src_dir
mkdir $project_src_dir/lib
mkdir $project_src_dir/lib/src
mkdir $project_src_dir/lib/include
touch $project_src_dir/lib/include/${project_name}.h
touch $project_src_dir/lib/src/${project_name}.cpp

cd $cmake_src_dir

echo "Preparing CMake file"

echo "CMAKE_MINIMUM_REQUIRED(VERSION $CURRENT_CMAKE_VERSION)" > $CMAKE_FILE
echo "PROJECT(${project_name})" >> $CMAKE_FILE
echo "" >> $CMAKE_FILE
echo "set(${project_name}_MAJOR_VERSION 0)" >> $CMAKE_FILE
echo "set(${project_name}_MINOR_VERSION 1)" >> $CMAKE_FILE
echo "" >> $CMAKE_FILE
echo "set(PROJECT_ROOT ${HOME}/proj/${project_name})" >> $CMAKE_FILE
echo "set(${project_name}_SRC \${PROJECT_ROOT}/lib/src)" >> $CMAKE_FILE
echo "set(${project_name}_INCLUDE \${PROJECT_ROOT}/lib/include)" >> $CMAKE_FILE
echo "" >> $CMAKE_FILE
echo "include_directories(\${${project_name}_INCLUDE})" >> $CMAKE_FILE
echo "" >> $CMAKE_FILE
echo "add_executable(${project_name} \${${project_name}_SRC}/${project_name}.cpp)" >> $CMAKE_FILE

echo "Finished preparing CMake file. Exiting..."
